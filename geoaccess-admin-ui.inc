<?php
/**
 * @file
 * Geoaccess core admin UI.
 */

function geoaccess_admin_general_form() {
  $form = array();

  return system_settings_form($form);
}

function geoaccess_admin_profile_list() {
  $form = array();

  $geoaccess_profiles = geoaccess_get_profiles();

  $profile_rows = array();
  if (count($geoaccess_profiles)) {
    foreach ($geoaccess_profiles as $dpid => $geoaccess_profile) {
      $profile_rows[] = array('data' => array($dpid, check_plain($geoaccess_profile["label"]),
        '<a href="' . url('admin/settings/geoaccess/edit_profile/' . $dpid)
                . '">' . t("Edit") . '</a>'));
    }
  }
  else {
    $profile_rows[] = array('data' => array(array('data' => t('No profiles defined'), 'colspan' => 3)));
  }

  $profiles_table = theme('table',
    array(t('Id'), t('Label'), t('Actions')),
    $profile_rows);

  $form['profiles_table'] = array('#value' => $profiles_table);

  return $form;
}

function geoaccess_admin_profile_form($form_id, $dpid) {
  $form = array();

  $form[] =
          array('#type' => 'item',
            '#title' => t('Profile id'),
            '#value' => $dpid > 0 ? $dpid : t('Unassigned'));

  $form['geoaccess_profile_id'] =
          array('#type' => 'hidden',
            '#value' => $dpid);

  // sanity check for $dpid
  $bad_dpid = FALSE;
  if (!isset($dpid) || !is_numeric($dpid) || ($dpid != -1 && $dpid < 1)) {
    $bad_dpid = TRUE;
  }

  if (!$bad_dpid) {
    if ($dpid > 0) {
      $geoaccess_profiles = geoaccess_get_profiles();
      if (isset($geoaccess_profiles[$dpid])) {
        $geoaccess_profile = $geoaccess_profiles[$dpid];
      }
      else {
        $bad_dpid = TRUE;
      }
      unset($geoaccess_profiles);
    }
    else {
      // defaults for new
      $geoaccess_profile = array(
        'blacklist' => 0,
        'countries' => array(),
        'content_types' => array('page' => 'Page'));
    }
  }

  if ($bad_dpid) {
    drupal_goto('admin/settings/geoaccess/list_profiles');
  }

  $form['label'] =
          array('#type' => 'textfield',
            '#title' => t('Profile label'),
            '#required' => TRUE,
            '#description' => t('The profile label. Used in views, links, etc'),
            '#default_value' => $geoaccess_profile['label']);

  $form['description'] =
          array('#type' => 'textarea',
            '#title' => t('Description'),
            '#description' => t('The description of the profile.'),
            '#default_value' => $geoaccess_profile['description']);

  $form['blacklist'] = array(
    '#title' => t('Blacklist behaviour'),
    '#type' => 'radios',
    '#default_value' => $geoaccess_profile['blacklist'],
    '#options' => array(
      t('Disabled'),
      t('Enabled'),
    ),
    '#description' => t('If blacklist is enabled access to nodes is allowed from all countries except for those selected.')
  );

  $form['content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Profile content types'),
    '#description' => t('Choose which content type to associate with this profile'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $options = node_get_types('names');

  $form['content_types']['content_types'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => $geoaccess_profile['content_types'],
  );

  $countries = geoaccess_country_values();
  unset($countries['A1']);
  unset($countries['A2']);
  asort($countries);

  $default_value = $geoaccess_profile['countries'];
  $form['countries'] = array(
    '#title' => t('Select countries need access control'),
    '#type' => 'checkboxes',
    '#default_value' => $default_value,
    '#options' => $countries,
  );

  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save country access profile'));
  if ($dpid > 0) {
    $form['buttons']['delete'] = array('#value' => l(t('Delete profile'), 'admin/settings/geoaccess/delete_profile/' . $dpid));
  }
  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }

  return $form;
}

function geoaccess_admin_profile_form_submit($form_id, $form) {
  $form_values = $form['values'];
  $dpid = (int) $form_values['geoaccess_profile_id'];

  if ($dpid == 0) {
    // internal error
    drupal_set_message(t('Internal error: geoaccess_profile_id=@dpid', array('@dpid' => $dpid)), 'error');
    return FALSE;
  }

  $geoaccess_profiles = geoaccess_get_profiles();

  if ($dpid < 0) {
    // get new $dpid
    $dpids = array_keys($geoaccess_profiles);
    if (count($dpids) > 0) {
      $dpid = 1 + max($dpids);
    }
    else {
      $dpid = 1;
    }
    $new_profile = TRUE;
  }
  else {
    $new_profile = FALSE;
  }

  // build profile array
  $geoaccess_profile = array();
  $geoaccess_profile['label'] = $form_values['label'];
  $geoaccess_profile['description'] = $form_values['description'];

  $geoaccess_profile['blacklist'] = (int) $form_values['blacklist'];

  $geoaccess_profile['countries'] = $form_values['countries'];

  $geoaccess_profile['content_types'] = $form_values['content_types'];

  $geoaccess_profiles[$dpid] = $geoaccess_profile;

  _geoaccess_set_profiles($geoaccess_profiles);
  drupal_set_message(t('Profile @dpid saved.', array('@dpid' => $dpid)));

  if ($new_profile) {
    // we need a jump, as this profile's edit url just changed
    drupal_goto('admin/settings/geoaccess/edit_profile/' . $dpid);
  }
}

function geoaccess_admin_delete_profile_form($form_id, $dpid) {
  $geoaccess_profiles = geoaccess_get_geoaccess_profiles();

  // sanity check for dpid
  if (!isset($geoaccess_profiles[$dpid])) {
    drupal_goto('admin/settings/geoaccess/list_profiles');
  }

  return confirm_form(array('geoaccess_profile_id' => array('#type' => 'hidden',
    '#value' => $dpid)),
    t('Are you sure you want to delete geoaccess profile @label (@id)?',
      array('@label' => $geoaccess_profiles[$dpid]['label'], '@id' => $dpid)),
          "admin/settings/geoaccess/edit_profile/" . $dpid, NULL,
    t('Delete'));
}

function geoaccess_admin_delete_profile_form_submit($form_id, $form) {
  $dpid = $form['values']['geoaccess_profile_id'];
  drupal_set_message(t("Deleted profile @id", array('@id' => $dpid)));

  $geoaccess_profiles = geoaccess_get_geoaccess_profiles();
  unset($geoaccess_profiles[$dpid]);
  _geoaccess_set_geoaccess_profiles($geoaccess_profiles);

  db_query("DELETE FROM {geoaccess} WHERE geoaccess_profile=%d", $dpid);

  drupal_goto("admin/settings/geoaccess/list_profiles");
}

/**
 * Return a list of country codes as specified by http://www.maxmind.com/app/iso3166
 */
function geoaccess_country_values() {
  static $countries = NULL;
  if (!isset($countries)) {
    module_load_include('inc', 'geoip', 'geoip.values');
    $countries = _geoip_country_values();
  }
  return $countries;
}
